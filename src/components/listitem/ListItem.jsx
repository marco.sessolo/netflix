import './listitem.scss';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import AddIcon from '@mui/icons-material/Add';
import ThumbUpOutlinedIcon from '@mui/icons-material/ThumbUpOutlined';
import ThumbDownOffAltOutlinedIcon from '@mui/icons-material/ThumbDownOffAltOutlined';
import { useState } from 'react';

function ListItem({ index }) {

  const [isHovered, setIsHovered] = useState(false);
  const trailer = 'https://ia802803.us.archive.org/15/items/nwmbc-Lorem_ipsum_video_-_Dummy_video_for_your_website/Lorem_ipsum_video_-_Dummy_video_for_your_website.mp4';

  return (
    <div className='listitem'
      style={{ left: isHovered && index * 225 - 50 + index * 2.5 }}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <img src="https://picsum.photos/200/300" alt="image" />
      {isHovered && (
        <>
          <video src={trailer} autoPlay={true} loop></video>
          <div className='itemInfo'>
            <div className='icons'>
              <PlayArrowIcon className='icon' />
              <AddIcon className='icon' />
              <ThumbUpOutlinedIcon className='icon' />
              <ThumbDownOffAltOutlinedIcon className='icon' />
            </div>
            <div className='itemInfoTop'>
              <span>1 hour 14 mins</span>
              <span className='limit'>+16</span>
              <span>1999</span>
            </div>
            <div className='desc'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum earum quasi iure nesciunt obcaecati minima non maxime laborum neque?</div>
            <div className='genre'>Action</div>
          </div>
        </>
      )}
    </div>
  )
}

export default ListItem;