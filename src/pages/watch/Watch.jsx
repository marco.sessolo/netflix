import './watch.scss';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';

function Watch() {
  return (
    <div className='watch'>
        <div className='back'>
            <ArrowBackOutlinedIcon />
            Home
        </div>
        <video className='video'
        autoPlay
        progress
        controls
        src='https://ia802803.us.archive.org/15/items/nwmbc-Lorem_ipsum_video_-_Dummy_video_for_your_website/Lorem_ipsum_video_-_Dummy_video_for_your_website.mp4' />
    </div>
  )
}

export default Watch;