import { useRef, useState } from 'react';
import './register.scss';
import { Link } from 'react-router-dom';

function Register() {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const emailRef = useRef();
    const passwordRef = useRef();

    const handleStart = () => {
        setEmail(emailRef.current.value)
    };

    const handleFinish = () => {
        setPassword(passwordRef.current.value)
    };

    return (
        <div className='register'>
            <div className='top'>
                <div className="wrapper">
                    <Link to='/' className='link'>
                        <img className='logo' src="https://upload.wikimedia.org/wikipedia/commons/0/08/Netflix_2015_logo.svg" alt="logo" />
                    </Link>
                    <Link to='/login'>
                        <button className="loginButton">Log In</button>
                    </Link>
                </div>
            </div>
            <div className="container">
                <h1>Unlimited Movies, TV shows, and more.</h1>
                <h2>Watch anywhere. Cancel anytime.</h2>
                <p>Ready to watch? Enter your email and to create or restart your membership.</p>
                {!email ? (
                    <div className="input">
                        <input type="email" placeholder='Email address' ref={emailRef} />
                        <button className='registerButton' onClick={handleStart}>Get Started</button>
                    </div>
                ) : (
                    <form className="input">
                        <input type="password" placeholder='Password' ref={passwordRef} />
                        <button className='registerButton' onClick={handleFinish}>Start</button>
                    </form>
                )}
            </div>
        </div>
    );
};

export default Register;